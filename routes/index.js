// ARCHIVO QUE SE EJECUTA AL LLAMAR BACKEND, EJECUTA LA LOGICA

var express = require('express');
var router = express.Router();
var rest = require("../mymodules/rest");

var tablaeventos=require("../models/event");
var tablaUsers=require("../models/users");

var buscareventos = function(request, response)  {
	
	tablaeventos.findAll().then(function(filas) { 
console.log(filas);
	var datosinfo = "";          
            for(event in filas){
            	var name = filas[event].name;

	console.log(event); 

        	console.log(name);     	
            	datosinfo += '<button type="button" style="text-align:center !important; padding-left:6px" class="btn btn-squared-default btn-primary text-left" data-container="body" data-html= "true" data-toggle="popover" data-placement="right" data-content="Estado: Todo Listo <br/>Fecha: Viernes<br/>Hora:9:00 pm <br/>Ubicación:Terraza<br/>Invitados:200<br/>Formal"  >\
            '+name+
      	 	 '</button>';
 			

            }
            response.end(datosinfo);
        });	

}  
 
var creareventos = function(request, response)  {
	var datosguardar = {
		name: request.body.name
	}  
	var mensaje = {
		exito: true, 
		message: "exito"
	}  
	var errorm = {
		exito: false, 
		message: "fracaso"
	}  
	tablaeventos.build(datosguardar).save().catch(function(fracaso){
	response.json(errorm);
	}).then(function(unafuncion){
		console.log("Success");

		response.json(mensaje);
	});



}  

/* GET home page. */
router.get('/', buscareventos


);
router.post('/', creareventos


);

router.get('/facebook_redirect', function(req, res, next) {
	//capturar datos de facebook 	
	var redirect_uri = "http://mydailyevent.com:3000/facebook_redirect";
	var facebook_code = req.query.code;
	var accesstoken_call = {
	    host: 'graph.facebook.com',
	    port: 443,
	    path: '/v2.10/oauth/access_token?client_id=354793484967340&redirect_uri='+redirect_uri+
	    '&client_secret=2081870ae988158e56c0fe8fb8113ab5&code='+facebook_code,
	    method: 'GET',
	    headers: {
	        'Content-Type': 'application/json'
	    }
	};
	var token;
	rest.getJSON(accesstoken_call, function(statusCode, access_token_response) {	    
	    var FB_path = '/me?fields=id,name,email,picture&access_token='+access_token_response.access_token;
	   
	    token=access_token_response.access_token;
	    console.log("FB_path: " + FB_path);
	    var userinfo_call = {
		    host: 'graph.facebook.com',
		    port: 443,
		    path: FB_path,
		    method: 'GET',
		    headers: {
		        'Content-Type': 'application/json'
		    }
		};
		rest.getJSON(userinfo_call, function(statusCode, user_info_response) {	    	
	    	console.log(user_info_response);
			tablaUsers.findOne( { where : { "facebookID" : user_info_response.id } } ).then(function(user){ //facebookID en tabla de usuarios y facebook access token
				if(user){
				user.facebook_access_token = token;
				user.facebook_code = user_info_response.facebook_code;
				user.save();}
				else{
			
  var current_time = new Date().getTime();
  var newUserData = {
  "facebook_access_token":token,
      "facebook_code":user_info_response.code,
       "facebookID":user_info_response.id,
      "email":user_info_response.email, 
      "name":user_info_response.name, 
      "username":user_info_response.username, 
      "password":user_info_response.password,
      "address":user_info_response.address, 
      "status":"active" 
  };
  console.log("Creando usuario...");
  console.log(newUserData);

    tablaUsers.build(newUserData).save().then(function(){
        //SUCCESS!! :)
    createJson = {"message":"Se ha creado el usuario con exito!", "created":true };          
        
    }).catch(function (err) {
        //ERROR!! :(
        console.log(err);
		createJson = {"message":"Ha ocurrido un error :(", "created":false };  				 
      
    });

}
			});
			res.redirect("http://mydailyevent.com/dailyevent/MainPage.html");  
		});
	});
});
router.post('/loginFB', function(req, res, next) {
	var redirect_uri = "http://mydailyevent.com:3000/facebook_redirect";
	res.redirect("https://www.facebook.com/v2.10/dialog/oauth?client_id=354793484967340&redirect_uri="+redirect_uri);  
});
router.get('/hey', function(req, res, next) {
  if(req.session.user){
    res.json({"message":"Hey dude!", "user":req.session.user})    
  }
  else{
    res.json({"message":"I'm sorry,... who are you???"})        
  }
});

router.post('/login', function(req, res, next) {	
	tablaUsers.findOne({
	  where: {username: req.body.username, password: sha256(req.body.password)}
	}).then(user => {
		if(user){
			req.session.user = user;
			res.json({"message":"Welcome "+user.name+"!"});			
		}else{
			res.json({"message":"Nah Nah Nah!"});					
		}
	}).catch(function(error){
		console.log("Whoops!, Something happened...");		
		console.log(error);		
	});	

});

router.get('/login', function(req, res, next) {	
	res.render("PAGINAEVENTOS");

});

module.exports = router;
