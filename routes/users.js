// var express = require('express');
// var router = express.Router();

// /* GET users listing. */
// router.get('/', function(req, res, next) {
//   res.send('respond with a resource');
// });

// module.exports = router;
var express = require('express');
var router = express.Router();

var tablaUsers = require('../models/users');

router.get('/', function(req, res, next) {
  tablaUsers.findAll().then(function(users) {
    res.json(users);
  });
});

router.post('/create', function(req, res, next) {
  var current_time = new Date().getTime();
  var newUserData = {

      
      "email":req.body.email, 
      "name":req.body.name, 
      "username":req.body.username, 
      "password":req.body.password,
      "address":req.body.address, 
      "status":"active" 
  };
  console.log("Creando usuario...");
  console.log(newUserData);

    tablaUsers.build(newUserData).save().then(function(){
        //SUCCESS!! :)
    createJson = {"message":"Se ha creado el usuario con exito!", "created":true };          
        res.json(createJson);
    }).catch(function (err) {
        //ERROR!! :(
        console.log(err);
		createJson = {"message":"Ha ocurrido un error :(", "created":false };  				 
        res.json(createJson);
    });


});


router.post('/login', function(req, res, next) {
  var responselogin = {};
  console.log(req.body);

  var query = {"email":req.body.email, "password":req.body.password };

  tablaUsers.findOne( { where: query } ).then(function(user)
	{
		if(user){
			responselogin = {"message":"Login exitoso", "login":true };  				 
		}else{
			responselogin = {"message":"Login fallido", "login":false };  	  	
		}
  		res.json(responselogin);
	});


});

module.exports = router;
