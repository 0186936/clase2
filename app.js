var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var sessions = require('express-session');



var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var index = require('./routes/index');
var users = require('./routes/users');

var app = express();
var allowCrossDomain = function(req, res, next) {
   res.header('Access-Control-Allow-Origin', '*');
   res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
   res.header('Access-Control-Allow-Headers', 'Content-Type');

   next();
}

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(allowCrossDomain);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/users', users);

app.use(sessions({  //declarar que vamos a usar una cookie
 cookieName: 'session', //acceder a datos de sesion
 secret: 'TBZ8teKiqJ7ypED0UcW4YEFiVIonGH788BOqPdlhNRJcF6LgoWv6bPZATnGUWXjKWZMdR51Sq1EB5Frk',
 duration: 24 * 60 * 60 * 1000, // ms   un dìa de duracion
 activeDuration: 1000 * 60 * 5 //5 min para extender
}));
//cookie: valor que se va a quedar en el explorador
//se guardan en un archivo en el servidor del backend

app.use('/app', express.static("../gitLunchFront"));   //Carpeta de front end o statics, en esta carpeta está todo lo público
//a6STmoSsaIek5X5guhf6

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
